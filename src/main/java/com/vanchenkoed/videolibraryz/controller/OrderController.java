package com.vanchenkoed.videolibraryz.controller;

import com.vanchenkoed.videolibraryz.model.Order;
import com.vanchenkoed.videolibraryz.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/order")
public class OrderController extends GenericController<Order> {

    protected OrderController(GenericRepository<Order> repository) {
        super(repository);
    }
    }

