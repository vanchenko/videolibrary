package com.vanchenkoed.videolibraryz.controller;

import com.vanchenkoed.videolibraryz.service.UserService;
import io.swagger.v3.oas.annotations.Hidden;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Slf4j
@Hidden
@Controller
@RequestMapping("/user")

public class MVCUserController {

    private final UserService service;

    public MVCUserController(UserService service) {
        this.service = service;
    }
    @GetMapping("/registration")
    public String registration(){
        return "registration";
    }
    @PostMapping("/registration")
    public String registration(@ModelAttribute("userForm")){
        return "redirect:login";
    }

}
