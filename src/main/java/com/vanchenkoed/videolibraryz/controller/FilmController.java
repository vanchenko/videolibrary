package com.vanchenkoed.videolibraryz.controller;


import com.vanchenkoed.videolibraryz.model.Film;
import com.vanchenkoed.videolibraryz.model.Genre;
import com.vanchenkoed.videolibraryz.repository.FilmRepository;
import com.vanchenkoed.videolibraryz.service.FilmService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@Slf4j
@RestController
@RequestMapping("/film")
public class FilmController extends GenericController<Film> {

    private final FilmService service;

    public FilmController(FilmService service) {
        super(service);
        this.service = service;
    }

    @GetMapping("/search")
    public List<Film> search(
            @RequestParam(value = "title", required = false) String title
    ) {
        return service.searchByTitle(title);
    }

}
