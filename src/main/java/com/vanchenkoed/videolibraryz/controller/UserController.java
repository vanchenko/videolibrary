package com.vanchenkoed.videolibraryz.controller;

import com.vanchenkoed.videolibraryz.model.User;
import com.vanchenkoed.videolibraryz.repository.GenericRepository;
import com.vanchenkoed.videolibraryz.security.JwtTokenUtil;
import com.vanchenkoed.videolibraryz.service.GenericService;
import com.vanchenkoed.videolibraryz.service.UserService;
import com.vanchenkoed.videolibraryz.service.userDetails.CustomUserDetailsService;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/user")
public class UserController extends GenericController<User> {

    private final UserService userService;

    private final JwtTokenUtil jwtTokenUtil;

    private final CustomUserDetailsService customUserDetailsService;

    protected UserController(UserService service) {

        super(service);
        this.userService = service;
        this.customUserDetailsService = customUserDetailsService;
        this.jwtTokenUtil = JwtTokenUtil;
    }

    @PostMapping("/auth")
    public ResponseEntity<?> auth(@RequestBody User user) {
        Map<String, Object> claims = new HashMap<>();
        if (!userService.checkPassword(User)) {
            return new ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Wrong password or unauthorized user")
        }
        UserDetails foundUser = customUserDetailsService.loadUserByUsername(user.getLogin());
        String token = jwtTokenUtil.generateToken(foundUser);

        response.put("token", token);
        response.put("authorities", foundUser.getAuthorities());
        return new ResponseEntity.ok().body(response);
    }
}
