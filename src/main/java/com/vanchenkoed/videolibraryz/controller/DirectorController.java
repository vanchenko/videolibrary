package com.vanchenkoed.videolibraryz.controller;


import com.vanchenkoed.videolibraryz.model.Director;
import com.vanchenkoed.videolibraryz.repository.GenericRepository;
import com.vanchenkoed.videolibraryz.service.DirectorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/director")
public class DirectorController extends GenericController<Director> {

    private final DirectorService service;

    public DirectorController(DirectorService service){
        super(service);
        this.service = service;
    }

}

