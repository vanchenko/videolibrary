package com.vanchenkoed.videolibraryz.controller;


import com.vanchenkoed.videolibraryz.model.GenericModel;
import com.vanchenkoed.videolibraryz.repository.GenericRepository;
import com.vanchenkoed.videolibraryz.service.GenericService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public abstract class GenericController<T extends GenericModel> {

    private final GenericService<T> service;
    protected GenericController(GenericService<T> service){
        this.service = service;
    }

    @Operation(description = "Получить список всех записей", method = "GetAll")
    @GetMapping("/list")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok().body(service.listAll());
    }

    @Operation(description = "Получить запись по id", method = "getOne")
    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") Long id){
        return ResponseEntity.status(HttpStatus.OK).body(service.getOne(id));
    }

    @Operation(description = "Создать запись", method = "Create")
    @PostMapping
    public ResponseEntity< ? > create(@RequestBody T object){
        return ResponseEntity.status(HttpStatus.CREATED).body(service.create(object));
    }

    @Operation(description = "Обновить запись", method = "Update")
    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody T object, @PathVariable Long id) {
        object.setId(id);
        return ResponseEntity.status(HttpStatus.OK).body(service.update(object));
    }
    @Operation(description = "Удалить запись", method = "Delete")
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        service.delete(id);
    }
}

