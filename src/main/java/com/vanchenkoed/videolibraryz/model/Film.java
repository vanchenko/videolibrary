package com.vanchenkoed.videolibraryz.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "film")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(name = "default_generator", sequenceName = "film_seq", allocationSize = 1)

public class Film extends GenericModel {

    @Column(name = "title")
    private String title;

    @Column(name = "premier_year")
    private String premier_year;

    @Column(name = "country")
    private String country;

    @Column(name = "genre")
    private Genre genre;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinTable(
            name = "film_directors",
            joinColumns = @JoinColumn(name= "film_id"),
            foreignKey = @ForeignKey(name ="FK_FILM_DIRECTORS"),
            inverseJoinColumns = @JoinColumn(name ="director_id"),
            inverseForeignKey = @ForeignKey(name ="FK_DIRECTORS_FILM")
    )

    private Set<Director> directors = new HashSet<>();

}
