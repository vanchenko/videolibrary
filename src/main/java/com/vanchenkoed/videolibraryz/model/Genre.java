package com.vanchenkoed.videolibraryz.model;

public enum Genre {
    FANTASY("Фантастика"),
    SCIENCE("Научный"),
    HORROR("Ужасы"),
    COMEDY("Комедии"),
    DRAMA("Драма"),
    DOCUMENTARY("Документальный");

    private final String genreText;

    Genre(String genreText){
        this.genreText = genreText;
    }

    private String getGenreText() {
        return this.genreText;
    }
}
