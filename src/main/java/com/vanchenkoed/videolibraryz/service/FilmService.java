package com.vanchenkoed.videolibraryz.service;

import com.vanchenkoed.videolibraryz.model.Film;
import com.vanchenkoed.videolibraryz.repository.FilmRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FilmService extends GenericService<Film> {

    private final FilmRepository repository;
    public FilmService(FilmRepository repository){
        super(repository);
        this.repository = repository;
    };


    public List<Film> searchByTitle(String title) {

        return repository.findAllByTitle(title);
    }

}
