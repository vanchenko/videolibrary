package com.vanchenkoed.videolibraryz.service;

import com.vanchenkoed.videolibraryz.model.Role;
import com.vanchenkoed.videolibraryz.repository.RoleRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleService {
    private final RoleRepository repository;

    public RoleService(RoleRepository repository){

        this.repository = repository;
    }

    public List<Role> getList(){

        return repository.findAll();
    }

    public Role getOne(Long id){

        return repository.findById(id).orElseThrow();
    }

}
