package com.vanchenkoed.videolibraryz.service;

import com.vanchenkoed.videolibraryz.model.Director;
import com.vanchenkoed.videolibraryz.repository.DirectorRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DirectorService extends GenericService<Director> {

    private final DirectorRepository directorRepository;

    public DirectorService(DirectorRepository repository, DirectorRepository directorRepository) {
        super(repository);
        this.directorRepository = directorRepository;
    }

    public List<Director> listAll() {

        return repository.findAll();
    }

    public List<Director> searchByAuthorFIO(String fio) {
        return directorRepository.findByAuthorFIO(fio);
    }

}
