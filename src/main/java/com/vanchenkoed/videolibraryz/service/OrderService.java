package com.vanchenkoed.videolibraryz.service;


import com.vanchenkoed.videolibraryz.model.Order;
import com.vanchenkoed.videolibraryz.repository.OrderRepository;
import org.springframework.stereotype.Service;

@Service
public class OrderService extends GenericService<Order> {

    private final OrderRepository repository;

    protected OrderService(GenericService<Order> repository) {

        super(repository);
    }

}
