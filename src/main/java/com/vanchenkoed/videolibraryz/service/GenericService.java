package com.vanchenkoed.videolibraryz.service;

import com.vanchenkoed.videolibraryz.model.GenericModel;
import com.vanchenkoed.videolibraryz.repository.GenericRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public abstract class GenericService<T extends GenericModel> {

    private final GenericRepository<T> repository;

    protected GenericService(GenericService<T> repository) {

        this.repository = (GenericRepository<T>) repository;
    }

    public List<T> listAll() {

        return repository.findAll();
    }

    public T getOne(Long id) {
        return repository.findById(id).orElseThrow();
    }

    public T create(T object) {

        return repository.save(object);
    }

    public T update(T object) {

        return repository.save(object);
    }

    public void delete(Long id) {

        repository.deleteById(id);
    }


    public T save(T object) {
        return repository.save(object);
    }
}
