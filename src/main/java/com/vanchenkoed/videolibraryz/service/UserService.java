package com.vanchenkoed.videolibraryz.service;

import com.vanchenkoed.videolibraryz.model.User;
import com.vanchenkoed.videolibraryz.repository.GenericRepository;
import com.vanchenkoed.videolibraryz.repository.RoleRepository;
import com.vanchenkoed.videolibraryz.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class UserService extends GenericService<User>{

    private final UserRepository repository;
    private final RoleService roleService;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    protected UserService(GenericRepository repository, BCryptPasswordEncoder bCryptPasswordEncoder){

        super(repository);
        this.repository = repository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;

        }
    @Override
    public User create(User user) {
        user.setCreatedBy("REGISTRATION");
        user.setRole(roleService.getOne(1L));
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        return repository.save(user);
    }

    public User createOperator(User user) {
        user.setCreatedBy("ADMIN");
        user.setRole(roleService.getOne(2L));
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        return repository.save(user);
    }

    public User getByLogin(String login){
        return repository.findUserByLogin(login);
    }

    public boolean checkPassword(User user){
        return bCryptPasswordEncoder.matches(User.getPassword(), getByLogin(user.getClass()));
    }

}


