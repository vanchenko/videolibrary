package com.vanchenkoed.videolibraryz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VideolibraryzApplication {

	public static void main(String[] args) {
		SpringApplication.run(VideolibraryzApplication.class, args);
	}

}
