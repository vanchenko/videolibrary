package com.vanchenkoed.videolibraryz.repository;


import com.vanchenkoed.videolibraryz.model.Film;
import com.vanchenkoed.videolibraryz.model.Genre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface FilmRepository extends GenericRepository<Film> {

    List<Film> findAllByName();

    List<Film> findAllByGenreOrTitle(Genre genre, String title);

    Set<Film> findAllByIdIn(Set<Long> ids);



}