package com.vanchenkoed.videolibraryz.repository;


import com.vanchenkoed.videolibraryz.model.Director;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface DirectorRepository extends GenericRepository<Director> {

    Set<Director> findAllByIdIn(Set<Long> ids);

    List<Director> findAllByDirectorFIO(String directorFIO);

}
