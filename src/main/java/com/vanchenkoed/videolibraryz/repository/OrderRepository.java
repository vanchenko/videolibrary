package com.vanchenkoed.videolibraryz.repository;


import com.vanchenkoed.videolibraryz.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends GenericRepository<Order> {
}
