package com.vanchenkoed.videolibraryz.repository;

import com.vanchenkoed.videolibraryz.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
}