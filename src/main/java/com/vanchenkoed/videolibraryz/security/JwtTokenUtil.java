package com.vanchenkoed.videolibraryz.security;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.Claims;


import java.util.Date;
import java.util.function.Function;

@Component
@RequiredArgsConstructor
public class JwtTokenUtil{

    private static final ObjectMapper getDefaultObjectMapper(){
        return new ObjectMapper();
    }

    public static final long JWT_TOKEN_VALIDITY = 7 * 24 * 60 * 60;

    public String getUserNameFromToken(String token){
        String subject = getClaimsFromToken(token, Claims::getSubject)
                JsonNode subjectJSON= null;
        try {
            subjectJSON = objectMapper.readTree(subject);
        } catch (JsonProcessingException e){
            e.printStackTrace();
        }
        if (subjectJSON != null) {
            return subjectJSON.get("username").asText();
        }else {
            return null;
        }
    }

    private <T> T getClaimsFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    public String generateToken(UserDetails userDetails){
        return doGenerateToken(userDetails.toString());
    }

    private String doGenerateToken(String subject){
        return Jwts.builder()
                .setSubject(subject)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY))
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    public Boolean validateToken(String token, UserDetails userDetails) {
        final Date expiration = getExperationDateFromToken(token);
        return expiration.before(new Date());
    }

    private Date getExperationDateFromToken(String token) {
        return getClaimsFromToken(token, Claims::getExperation);
    }
}
